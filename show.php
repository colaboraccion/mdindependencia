<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<ul class="nav nav-tabs">
				<li class="nav-item">
					<a id="camara" class="nav-link active" href="#">Camaras</a>
				</li>
				<li class="nav-item">
					<a id="dvehiculo" class="nav-link" href="#">Densidad Vehicular</a>
				</li>
				<li class="nav-item">
					<a id="pvelocidad" class="nav-link" href="#">Promedio de velocidad</a>
				</li>
			</ul>
			<div id="content_dashboard" style="margin:10px">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">Camara Demostrativa</div>
							<div id="m_buscador" class="panel-body">
								<!-- <div class="row">
									<div class="col-lg-6">
										<div class="row">
											<div class="col-lg-6">
												<div class="panel panel-default" style="margin-bottom:0px">
													<div class="panel-heading">Cámara 1 fija</div>
													<div class="panel-body" style="padding:0px">
														<iframe src="http://200.37.252.189:2000/172.16.5.2" style="width:170px; height:140px" frameborder="0" scrolling="no"></iframe>
													</div>
												</div>
												<div class="panel panel-default" style="margin-bottom:0px">
													<div class="panel-heading">Cámara 2 fija</div>
													<div class="panel-body" style="padding:0px">
														<iframe src="http://200.37.252.189:2000/172.16.5.6" style="width:170px; height:140px" frameborder="0" scrolling="no"></iframe>
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="panel panel-default" style="margin-bottom:0px">
													<div class="panel-heading">Cámara 3 fija</div>
													<div class="panel-body" style="padding:0px">
														<iframe src="http://200.37.252.189:2000/172.16.5.1" style="width:170px; height:140px" frameborder="0" scrolling="no"></iframe>
													</div>
												</div>
												<div class="panel panel-default" style="margin-bottom:0px">
													<div class="panel-heading">Cámara 4 fija</div>
													<div class="panel-body" style="padding:0px">
														<iframe src="http://200.37.252.189:2000/172.16.5.10" style="width:170px; height:140px" frameborder="0" scrolling="no"></iframe>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="panel panel-default" style="margin-bottom:0px">
											<div class="panel-heading">Cámara Domo PTZ</div>
											<div class="panel-body" style="padding:0px">
												<iframe src="http://200.37.252.189:2000/172.16.5.5" style="width:329px; height:250px" frameborder="0" scrolling="no"></iframe>
											</div>
										</div>
									</div>
								</div> -->
								<div class="row">
									<div class="col-lg-12">
										<table class="table table-hover">
											<tbody>
												<tr>
													<td style="width:303px"><b>Cámara Fija 1</b></td>
													<td style="width:247px"><b>Cámara Fija 2</b></td>
													<td style="width:382px"><b>Cámara Domo PTZ</b></td>
												</tr>
												<tr>
													<td colspan="2" style="padding:0px;margin:0px;">
														<iframe src="http://200.37.252.189:2000/172.16.5.2" style="width: 239px;height: 106px;" frameborder="0" scrolling="no"></iframe>
														<iframe src="http://200.37.252.189:2000/172.16.5.6" style="width: 218px;height: 106px;" frameborder="0" scrolling="no"></iframe>
													</td>
													<td rowspan="2" style="padding:0px; margin:0px">
														<iframe src="http://200.37.252.189:2000/172.16.5.5" style="width: 386px;height: 257px;" frameborder="0" scrolling="no"></iframe>
													</td>
												</tr>
												<tr>
													<td colspan="2" style="padding:0px;margin:0px ">
													<iframe src="http://200.37.252.189:2000/172.16.5.10" style="width: 239px;height: 129px;" frameborder="0" scrolling="no"></iframe>
														<iframe src="http://200.37.252.189:2000/172.16.5.8" style="width: 218px;height: 129px;" frameborder="0" scrolling="no"></iframe>
													</td>
												</tr>
												<tr>
													<td><b>Cámara Fija 3</b></td>
													<td><b>Cámara Fija 4</b></td>
													<td></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>